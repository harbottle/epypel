FROM centos:7
LABEL maintainer="harbottle <harbottle@room3d3.com>"
# Update and repos
RUN yum -y update \
  && yum -y install epel-release \
  && yum -y install http://harbottle.gitlab.io/harbottle-main-release/harbottle-main-release-7.rpm \
  && yum clean all \
  && rm -rf /var/cache/yum
# Compiler tools
RUN yum -y install gcc gcc-c++ make \
  && yum clean all \
  && rm -rf /var/cache/yum
# RPM tools
RUN yum -y install rpm-build rpm-sign createrepo \
  && yum clean all \
  && rm -rf /var/cache/yum
# Utils
RUN yum -y install git nodejs-bower wget \
  && yum clean all \
  && rm -rf /var/cache/yum
# Libraries
RUN yum -y install atlas-devel lapack-devel libffi-devel libpcap-devel \
  lxc-devel openblas-devel openssl-devel postgresql-devel zlib-devel \
  libxml2-devel libxslt-devel \
  && yum clean all \
  && rm -rf /var/cache/yum
# Ruby tools
RUN yum -y install ruby-devel rubygem-bundler \
  && yum clean all \
  && rm -rf /var/cache/yum
# python2.7
RUN yum -y install python-devel python-pip python-setuptools numpy scipy \
  && yum clean all \
  && rm -rf /var/cache/yum
# python3.4
RUN yum -y install python34-devel python34-pip python34-setuptools \
  python34-Cython python34-numpy python34-numpy-f2py \
  && yum clean all \
  && rm -rf /var/cache/yum
# python3.6
RUN yum -y install python3-wheel python36-devel python36-pip \
  python36-setuptools python36-Cython python36-numpy python36-numpy-f2py \
  python36-simplejson python36-scipy \
  && yum clean all \
  && rm -rf /var/cache/yum
RUN pip3.6 install --upgrade setuptools
