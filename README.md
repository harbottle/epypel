# **epypel**: Extra Python Packages for Enterprise Linux

**[Browse the yum repo.](https://harbottle.gitlab.io/epypel/7/x86_64)**

A yum repo of RPM files containing Python
[packages](https://pypi.python.org/pypi) not available in the standard repos.
The packages are suitable for CentOS 7 (and
RHEL, Oracle Linux, etc.). Ensure you also have the
[EPEL](https://fedoraproject.org/wiki/EPEL) repo enabled.

Packages are converted automatically using
[GitLab CI](https://about.gitlab.com/gitlab-ci/) and the excellent
[fpm](https://github.com/jordansissel/fpm) gem.  The yum repo is hosted
courtesy of [GitLab Pages](https://pages.gitlab.io/).

## Quick Start

```bash
# Install the EPEL repo
sudo yum -y install epel-release

# Install the epypel repo
sudo yum -y install https://harbottle.gitlab.io/epypel/7/x86_64/epypel-release.rpm
```

After adding the repo to your system, you can install
[available packages](https://harbottle.gitlab.io/epypel/7/x86_64) using `yum`.

## Why?
epypel is an easy way to install Python packages on your CentOS box using the
native `yum` package manager.  Additional packages can be easily added to the
[`packages.yml`](packages.yml) file to expand the repo in line with user needs.

## Adding packages

[File a new issue](https://gitlab.com/harbottle/epypel/issues/new).
